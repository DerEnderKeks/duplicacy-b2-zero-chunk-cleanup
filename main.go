package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"sync"
)

type file struct {
	ID   string `json:"fileId"`
	Name string `json:"fileName"`
	Size int    `json:"contentLength"`
}

type respList struct {
	Files    []file `json:"files"`
	NextName string `json:"nextFileName"`
	NextID   string `json:"nextFileId"`
}

func main() {
	b2KeyID := os.Getenv("B2_KEY_ID")
	b2KeyToken := os.Getenv("B2_KEY_TOKEN")
	b2BucketID := os.Getenv("B2_BUCKET_ID")

	req, err := http.NewRequest("GET", "https://api.backblazeb2.com/b2api/v2/b2_authorize_account", nil)
	if err != nil {
		panic(err)
	}
	req.SetBasicAuth(b2KeyID, b2KeyToken)

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}

	authResp := struct {
		ApiURL    string `json:"apiUrl"`
		AuthToken string `json:"authorizationToken"`
	}{}

	err = json.NewDecoder(resp.Body).Decode(&authResp)

	if err != nil {
		panic(err)
	}

	fileIndex := make(map[string][]file)

	nextName := ""
	nextID := ""
	for {
		body := struct {
			BucketId      string `json:"bucketId"`
			MaxFileCount  int    `json:"maxFileCount"`
			Prefix        string `json:"prefix"`
			StartFileName string `json:"startFileName,omitempty"`
			StartFileId   string `json:"startFileId,omitempty"`
		}{
			BucketId:      b2BucketID,
			MaxFileCount:  10000,
			Prefix:        "chunks/",
			StartFileName: nextName,
			StartFileId:   nextID,
		}

		jsonBody, err := json.Marshal(body)
		if err != nil {
			panic(err)
		}

		req, err := http.NewRequest("POST", authResp.ApiURL+"/b2api/v2/b2_list_file_versions", bytes.NewBuffer(jsonBody))
		if err != nil {
			panic(err)
		}
		req.Header.Add("Authorization", authResp.AuthToken)

		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}

		respParsed := respList{}

		err = json.NewDecoder(resp.Body).Decode(&respParsed)
		if err != nil {
			panic(err)
		}

		for _, f := range respParsed.Files {
			fileIndex[f.Name] = append(fileIndex[f.Name], f)
		}

		nextName = respParsed.NextName
		nextID = respParsed.NextID

		fmt.Printf("\r%d files fetched", len(fileIndex))
		if len(nextName) == 0 {
			fmt.Println("")
			break
		}
	}

	var deletedFiles Counter
	var failedFiles Counter

	jobs := make(chan bool, 25)
	var wg sync.WaitGroup

	for _, files := range fileIndex {
		if len(files) < 2 {
			continue
		}

		for _, f := range files {
			if f.Size > 0 {
				continue
			}

			jobs <- true
			f := f
			wg.Add(1)
			go func() {
				defer func() {
					<-jobs
					wg.Done()
				}()
				deleteFile(f, authResp.ApiURL, authResp.AuthToken, client, &deletedFiles, &failedFiles)
			}()
		}
	}
	wg.Wait()

	fmt.Printf("Deleted: %d, Failed: %d", deletedFiles.Value(), failedFiles.Value())
}

func deleteFile(f file, apiURL, authToken string, client *http.Client, deletedFiles *Counter, failedFiles *Counter) {
	body := struct {
		FileName string `json:"fileName"`
		FileId   string `json:"fileId"`
	}{
		FileName: f.Name,
		FileId:   f.ID,
	}

	jsonBody, err := json.Marshal(body)
	if err != nil {
		panic(err)
	}

	req, err := http.NewRequest("POST", apiURL+"/b2api/v2/b2_delete_file_version", bytes.NewBuffer(jsonBody))
	if err != nil {
		panic(err)
	}
	req.Header.Add("Authorization", authToken)

	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	prefix := fmt.Sprintf("Deleting %s: %s... ", f.Name, f.ID)
	if resp.StatusCode == 200 {
		deletedFiles.Add(1)
		fmt.Printf("%sOk\n", prefix)
	} else {
		failedFiles.Add(1)
		text, _ := ioutil.ReadAll(resp.Body)
		fmt.Printf("%sFailed: %v\n", prefix, text)
	}
}

// Counter stolen from https://stackoverflow.com/questions/10728863/how-to-lock-synchronize-access-to-a-variable-in-go-during-concurrent-goroutines
type Counter struct {
	mu sync.Mutex
	x  int64
}

func (c *Counter) Add(x int64) {
	c.mu.Lock()
	c.x += x
	c.mu.Unlock()
}

func (c *Counter) Value() (x int64) {
	c.mu.Lock()
	x = c.x
	c.mu.Unlock()
	return
}
