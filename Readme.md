# Duplicacy B2 Zero Chunk Cleanup

This "script" deletes 0 byte duplicates of Duplicacy backup chunks from Backblaze B2, which sometimes mysteriously appear for some reason.

## Usage

Requires Go compiler.

This program assumes that the backups are in the root path of the B2 Bucket (it searches for chunks in `/chunks/`).

```
B2_KEY_ID=<Key ID> B2_KEY_TOKEN=<Key Token> B2_BUCKET_ID=<Bucket ID> go run .
```

Depending on how many chunks are in the bucket and more importantly how many are duplicates this might take a while.